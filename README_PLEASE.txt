####################    Docker BDD    ####################  

// build ( dans le repertoire du dockerfile de la bdd )
docker build -t bdd .

// run avec password, volumes et port
docker run --name d21-postgres-instance1 -p 8085:80 -p 5432:5432 -v www:/var/www/ -v postgresql_data:/var/lib/postgresql/data/ -e POSTGRES_PASSWORD=3eb48u -d bdd


####################  Docker Compose   #################### 

Il y a 2 docker-compose dans le rendu du projet.
	- le premier simple, fonctionnel avec les servers et la BDD s�par�e (utilis� le jour de la soutenance ) . 
	- le deuxi�me complet, les servers + BDD + clientWebsocket mais non fonctionnel le jour de la soutenance.


####################     WebSocket     ####################

Le rendu du projet contient un screen avec le demonstration de la websocket.
On y voit le serverRest avec la bdd en haut ( ajout bien r�alis� )
Le client Websocket ( java int�gr� ) en bas � gauche.
Le serverWebSocket en bas a droite. Le screen montre les communications entre les 2 dernier cit�s.


####################      Guide        ####################  

Dossier BrunoProjet     -> server Rest
Dossier ServerWebSocket -> server WebSocket
Dossier ClientWeb       -> clientWebsocket + java int�gr�

Dossier ClientWebSocket -> clientWebsocket (sans client java ) n'est pas compil� lors du test, mais utilis� par le Dossier ClientWeb.

Pr�cisions : J'utilise , pour le client Android, des JSONDecoder d'objets JSON, et de listes d'objets JSON.
Pour la BDD, j'utilise JPA ainsi qu'une DAO G�n�rique et "QueryParameter" pour les requetes nomm�es.